"""YtApiProj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from YtApiApp import views
from django.conf import settings
from django.conf.urls.static import static

# handler400 = "YtApiApp.views.handler400"; # Do not use => views.handler400 => otherwise => "No module named views"
# handler403 = "YtApiApp.views.handler403";
handler404 = "YtApiApp.views.handler404";
handler500 = "YtApiApp.views.handler500";
handler403 = "YtApiApp.views.handler403";
handler400 = "YtApiApp.views.handler400"; # Do not use => views.handler400 => otherwise => "No module named views"


urlpatterns = [
    # Django JET urls
    url(r"jet/", include("jet.urls", "jet")), 
    # Django JET dashboard's urls
    url(r"jet/dashboard/", include("jet.dashboard.urls", "jet-dashboard")),

    url(r'^admin/', admin.site.urls),
    url(r"^$", views.index, name="index"),
    url(r"^index-later$", views.index_later, name='index-later'),
    url(r"^oauth2Callback$", views.oauth2_callback, name="oauth2_callback"),
    url(r"^trial/holoviews/(?P<id>\d+(\.\d+)*)$", views.trial, name="trial"),
    url(r"^show-pdf$", views.show_pdf, name="show_pdf"),
    url(r"^calc$", views.calc, name="calc"),
    url(r"^stk/read-json", views.read_json, name='read-json'),
    # url(r"^lines$", views.lines, name="lines"),
    # url(r"^color-scatter$", views.color_scatter, name="color-scatter"),
    # url(r"^linked-panning$", views.linked_panning, name="linked-panning"),
    # url(r"^twin-axis$", views.twin_axis, name="twin-axis"),
    # url(r"^lorenz$", views.lorenz, name="lorenz"),
    # url(r"^burtin$", views.burtin, name="burtin"),
    url(r"^bokeh/plots/(?P<sub_url>[\w-]+)$", views.show_plots, name="show_plots"),
    url(r"^holoviews/plots/(?P<sub_url>[\w-]+)$", views.show_holoviews_plots, name="show_holoviews_plots"),
    url(r'^jupyter-notebooks/python-lists/$', views.show_jupyter_notebooks, name="show-jupyter-notebooks"),
    url(r"^jupyter-notebooks/holoviews-plots/$", views.show_jupyter_notebooks_holoviews_plots, name="holoviews-plots"),
    url(r"^jupyter-notebooks/holomaps/$", views.show_jupyter_notebooks_holomaps, name="holomaps"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
