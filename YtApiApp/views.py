# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import FileResponse, Http404, HttpResponse
from .models import IframeSubUrl, HoloviewsSubUrl

# Create your views here.

def index(request):
	plots = IframeSubUrl.objects.all()
	for plot in plots:
		print plot.sub_url, plot.plot_name
		
	holoviews = HoloviewsSubUrl.objects.all()
	for plot in holoviews:
		print plot.sub_url, plot.plot_name
	return render(request, "index.html", {"plots": plots, "holoviews": holoviews})

def index_later(request):
	return render(request, 'index-later.html', {});

def oauth2_callback(request):
	return render(request, "oauth2Callback.html", {});

def calc(request):
	return render(request, "calc.html", {});

def read_json(request):
	return render(request, "stk/read_json.html", {});

def trial(request, id):
	print id
	# return render(request, "trial.html", {});
	# return render(request, "holoviews_trial/trial3.html", {});
	# 4.1 is working
	return render(request, "holoviews_trial/trial"+ str(id) + ".html", {});


# def render_tested_bokeh_holoviews_pages(self, id):
# 	return render(request, "holoviews_trial/trial4.4.html", {})
def lines(request):
	return render(request, "lines.html", {});

def color_scatter(request):
	return render(request, "color_scatter.html", {});

def linked_panning(request):
	return render(request, "linked_panning.html", {});

def twin_axis(request):
	return render(request, "twin_axis.html", {});

def lorenz(request):
	return render(request, "lorenz.html", {});

def burtin(request):
	return render(request, "burtin.html", {});

def show_plots(request, sub_url):
	try:
		print "Rendering it"
		print sub_url, type(sub_url)
		return render(request, "plots/" + sub_url + ".html", {})
	except:
		print "404 - page not found"
		# raise HttpResponse(status=404) 
		# raise Http404("<h1 style='color:red'>Could not found the requested page</h1>")
		raise Http404("Could not found the requested page")

def show_holoviews_plots(request, sub_url):
	try:
		print "Rendering Holoviews"
		print sub_url, type(sub_url)
		return render(request, "holoviews/" + sub_url + ".html", {})
	except:
		print "404 - page not found"
		# raise HttpResponse(status=404) 
		# raise Http404("<h1 style='color:red'>Could not found the requested page</h1>")
		raise Http404("Could not found the requested page")

def show_pdf(request):
	print "PDF view"
	try:
		return FileResponse(open("complex_report_dell_logo.pdf", "rb"), content_type="application/pdf")
	except Exception as err:
		print err
		raise Http404

def show_jupyter_notebooks(request):
	return render(request, 'jupyter-notebooks/python-lists.html', {});
	# return render(request, 'jupyter-notebooks/python-lists.ipynb', {});

def show_jupyter_notebooks_holomaps(request):
	return render(request, 'jupyter-notebooks/holomaps.html', {});

def show_jupyter_notebooks_holoviews_plots(request):
	return render(request, 'jupyter-notebooks/holoviews-plots2.html', {});
	
# Custom pages

# def handler400(request):
# 	return render(request, "400.html", {})

# def handler403(request):
# 	return render(request, "403.html", {})

def handler404(request):
	return render(request, "404.html", {})

def handler500(request):
	return render(request, "500.html", {})

def handler403(request):
	return render(request, "403.html", {})

def handler400(request):
	return render(request, "400.html", {})





