# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# Bokeh apps
class IframeSubUrl(models.Model):
	sub_url = models.CharField(max_length=30, null=False, blank=False);
	plot_name = models.CharField(max_length=30, null=False, blank=False);

	def __str__(self):
		return "Sub url: {}, Plot name: {}".format(self.sub_url, self.plot_name)

# Holoviews
class HoloviewsSubUrl(models.Model):
	sub_url = models.CharField(max_length=30, null=False, blank=False);
	plot_name = models.CharField(max_length=30, null=False, blank=False);
	doc_url = models.CharField(max_length=500, default='')
	created = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return "Sub url: {}, Plot name: {}".format(self.sub_url, self.plot_name)	